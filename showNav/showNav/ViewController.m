//
//  ViewController.m
//  showNav
//
//  Created by dihuijun on 17/5/8.
//  Copyright © 2017年 Cycle. All rights reserved.
//

#import "ViewController.h"
#import "tableHeaderView.h"
#import "CycleCameraAndLibraryVC.h"


#define kwidth [UIScreen mainScreen].bounds.size.width
#define navWH 20
#define headImgWH 100
@interface ViewController ()<UITableViewDataSource, UITableViewDelegate>
//{
//    UIView *navBarView;
//    UIButton *rightbtnShare;
//    UIButton *rightbtnSetting;
//    UILabel *titleLabel;
//}
//
@property(nonatomic, strong)UITableView     *tableView;
@property(nonatomic, strong)tableHeaderView     *headerView;

//@property(nonatomic, strong)UIImageView     *headerBackView;        // 头像背景图片
//@property(nonatomic, strong)UIImageView     *photoImageView;        // 头像图片
//@property(nonatomic, strong)UILabel         *userNameLabel;         // 用户名label
//@property(nonatomic, strong)UILabel         *introduceLabel;        // 简介label
//@property(nonatomic, strong)UIView          *tableViewHeaderView;   // tableView头部视图
@property(nonatomic, assign)NSInteger       imageHeight;            // 背景图片的高

@end

@implementation ViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    ///1.创建tableView
    [self setupTableView];
    
    ///2.导航栏处
    [self setupNav];
       
 
}
#pragma mark --- 1.setupTableView
- (void)setupTableView
{
    
    _tableView = [[UITableView alloc] initWithFrame:self.view.frame style:UITableViewStylePlain];
    _tableView.dataSource = self;
    _tableView.delegate = self;
    [self.view addSubview:_tableView];
}
#pragma mark --- 2.导航栏处
- (void)setupNav{
    // 背景图片的高度
    _imageHeight = 240;
    ///设置导航栏隐藏
    self.navigationController.navigationBar.hidden = YES;
    self.view.backgroundColor = [UIColor whiteColor];
    
    //2.1 创建自定义headerView
    self.headerView  = [[tableHeaderView alloc]initWithFrame:CGRectMake(0, 0, kwidth, _imageHeight)];
    self.headerView.backgroundColor = [UIColor whiteColor];
    
    //2.2 把储存好的需要添加到父容器的控件取出来添加到父容器上
    for (NSInteger i=0; i<self.headerView.childArray.count; i++) {
        //添加
        [self.view addSubview:self.headerView.childArray[i]];
    }
    
    //点击设置和分享
    __weak typeof(self) wself = self;
    self.headerView.shareblock = ^(){
        NSLog(@"点击了分享");
        
        [wself clickShare:@"分享啥呢"];
    };
    self.headerView.settingblock = ^(){
        NSLog(@"点击了设置");
        //清理缓存为例子
        [wself clearCache1];
    };
    //头像
    self.headerView.vc = self;//照相需要
    //2.3 把headerView添加到表格头部
    self.tableView.tableHeaderView = self.headerView;
}







#pragma mark ---- 表格代理
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat width = self.view.frame.size.width;// 图片的宽度
    CGFloat yOffset = scrollView.contentOffset.y;// 偏移的y值
    NSLog(@"%f",yOffset);
    if (yOffset < 0) {
        CGFloat totalOffset =
      _imageHeight  + ABS(yOffset);
        
        //page
        CGFloat page = totalOffset / _imageHeight;
        
        // 拉伸后的图片的frame应该是同比例缩放
        self.headerView.headerBackView.frame = CGRectMake(- (width * page - width) / 2, yOffset, width * page, totalOffset);
        
        // 拉伸后头像宽度
        CGFloat photoImgWidth=headImgWH+(-yOffset-navWH);
        // 头像放大
        CGFloat imgy = 50-(-yOffset-navWH);
        NSLog(@"---:%f",imgy);
        self.headerView.photoImageView.frame=CGRectMake((kwidth - photoImgWidth) / 2, imgy, photoImgWidth, photoImgWidth);
        self.headerView.photoImageView.layer.cornerRadius = photoImgWidth/2;
        self.headerView.photoImageView.layer.masksToBounds = YES;
    }
    
    
    if (yOffset>122.5) {//也就是下拉
        self.headerView.titleLabel.hidden = NO;
        self.headerView.titleLabel.text = self.headerView.userNameLabel.text;
        self.headerView.navBarView.alpha = 1.0;
        
    }else{
        self.headerView.titleLabel.hidden = YES;
        self.headerView.navBarView.alpha = 0.0;
    }
   
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 30;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    return cell;
}



#pragma mark --- 点击设置清理缓存
- (void)clearCache1
{
    
    
    __weak typeof(self) wself1 = self;
    
    
    
    
    
    CGFloat cacheSize = [CycleTool folderSizeAtPath];
    NSString *message = [NSString stringWithFormat:@"缓存大小为%.2fM,确定要清理缓存吗？",cacheSize];
    
    
    CKAlertViewController *alertViewController = [CKAlertViewController alertControllerWithTitle:@"温馨提示" message:message];
    alertViewController.messageAlignment = NSTextAlignmentLeft;
    CKAlertAction *cancel = [CKAlertAction actionWithTitle:@"取消" handler:^(CKAlertAction *action) {
        NSLog(@"取消");
    }];
    CKAlertAction *sure = [CKAlertAction actionWithTitle:@"确定" handler:^(CKAlertAction *action) {
        NSLog(@"确定");
        [SVProgressHUD showWithStatus:@"正在清理..."];
        [wself1 clean];
        
    }];
    [alertViewController addAction:cancel];
    [alertViewController addAction:sure];
    [self presentViewController:alertViewController animated:NO completion:nil];
    
    
    
    
    
    
    
}
- (void)clean{
    [CycleTool cleanCache:^{
        NSLog(@"清理成功");
        [SVProgressHUD showSuccessWithStatus:@"清理完毕"];
    }];
}

#pragma mark --- 点击分享
- (void)clickShare:(NSString *)shareMessage{
    CKAlertViewController *ckAlert = [CKAlertViewController alertControllerWithTitle:@"温馨提示" message:shareMessage];
    ckAlert.messageAlignment = NSTextAlignmentLeft;
    CKAlertAction *cancel = [CKAlertAction actionWithTitle:@"取消" handler:^(CKAlertAction *action) {
        NSLog(@"取消");
    }];
    CKAlertAction *sure = [CKAlertAction actionWithTitle:@"分享" handler:^(CKAlertAction *action) {
        NSLog(@"分享");
    }];
    
    [ckAlert addAction:cancel];
    [ckAlert addAction:sure];
    
    
    [self presentViewController:ckAlert animated:NO completion:nil];
    
    
}










@end
