//
//  tableHeaderView.m
//  showNav
//
//  Created by dihuijun on 17/5/9.
//  Copyright © 2017年 Cycle. All rights reserved.
//

#import "tableHeaderView.h"
//拍照
/** ----------------------        头像相关    ----------   ***/
#import "CycleCameraAndLibraryVC.h"
#import "ViewController.h"
#import "SCAvatarBrowser.h"
/** ----------------------       / 头像相关    ----------   ***/

#define kcolor [UIColor colorWithRed:27/255.0 green:127/255.0 blue:57/255.0 alpha:1.0]
#define kwidth [UIScreen mainScreen].bounds.size.width
#define navWH 20
#define headImgWH 100

@interface tableHeaderView ()<CycleCameraAndLibraryVCDelegate>
@property (strong, nonatomic) VWWWaterView *wview;
@end

@implementation tableHeaderView
/**   array   */
- (NSMutableArray *)childArray{
    if(!_childArray){
        _childArray = [NSMutableArray array];
    }
    return _childArray;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.frame = frame;
        //1.createChildren
        [self setupChildren];
    }
    return self;
}
- (void)setupChildren{
    /** 1. 头部 */
    self.navBarView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kwidth, 54)];
    self.navBarView.backgroundColor = kcolor;
//    [self.view addSubview:self.navBarView];
    self.navBarView.alpha = 0.0;
    //1.1 右边按钮(分享)
    CGFloat margin = 24.00;
    
    self.rightbtnSetting = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [self.rightbtnSetting setImage:[UIImage imageNamed:@"setting1"] forState:UIControlStateNormal];
    self.rightbtnSetting.frame = CGRectMake(self.frame.size.width-margin-navWH, margin, navWH, navWH);
    //点击事件
    [self.rightbtnSetting addTarget:self action:@selector(ClickSetting:) forControlEvents:UIControlEventTouchUpInside];
//    [self.view  addSubview:self.rightbtnShare];
    
    
    
    
    
    //1.2 右边按钮(分享)
    self.rightbtnShare = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [self.rightbtnShare setImage:[UIImage imageNamed:@"sahre4"] forState:UIControlStateNormal];
    self.rightbtnShare.frame = CGRectMake(self.frame.size.width-2*(margin+navWH), margin, navWH, navWH);
    
    [self.rightbtnShare addTarget:self action:@selector(ClickShare:) forControlEvents:UIControlEventTouchUpInside];
//    [self.view addSubview:self.rightbtnSetting];
    
    
    
    //1.3 标题
    self.titleLabel = [UILabel new];
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.navBarView addSubview:self.titleLabel];
    self.titleLabel.frame = CGRectMake((kwidth-headImgWH)*0.5, margin, headImgWH, navWH);
    
    //添加到数组
    [self.childArray addObject:self.navBarView];
    [self.childArray addObject:self.rightbtnShare];
    [self.childArray addObject:self.rightbtnSetting];
    
    
    
    /***** 身体部分 ****/
    // 背景图
    _headerBackView = [[UIImageView alloc] init];
    _headerBackView.frame = CGRectMake(0, 0, kwidth, self.frame.size.height);
    _headerBackView.image = [UIImage imageNamed:@"bg"];
    [self addSubview:_headerBackView];
    
    // 头像
    _photoImageView = [[UIImageView alloc] initWithFrame:CGRectMake((kwidth - headImgWH) / 2, headImgWH*0.5, headImgWH, headImgWH)];
    [self addSubview:self.photoImageView];
    _photoImageView.layer.cornerRadius = headImgWH*0.5;
    _photoImageView.layer.masksToBounds = YES;
    _photoImageView.image = [UIImage imageNamed:@"head"];
    //点击头像
    _photoImageView.userInteractionEnabled = YES;
    [_photoImageView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(ClickHead:)]];
    
    
    
    // 用户名
    _userNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_photoImageView.frame) + 10, kwidth, navWH)];
    _userNameLabel.font = [UIFont systemFontOfSize:16];
    _userNameLabel.text = @"Mac";
    _userNameLabel.textAlignment = 1;
    _userNameLabel.textColor = [UIColor whiteColor];
    [self addSubview:self.userNameLabel];
    
    // 简介
    _introduceLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_userNameLabel.frame) + 10, kwidth, navWH)];
    _introduceLabel.alpha = 0.7;
    _introduceLabel.text = @"这家伙很赖，什么也没留下。";
    _introduceLabel.textAlignment = 1;
    _introduceLabel.font = [UIFont systemFontOfSize:12];
    _introduceLabel.textColor = [UIColor whiteColor];
    [self addSubview:_introduceLabel];

    //3.water
    [self setupWater];
}



/**
 *  点击分享
 */
- (void)ClickShare:(UIButton *)shareButton{
//    NSLog(@"分享");
    !self.shareblock ?: self.shareblock();
}


/**
 *  点击设置
 */
- (void)ClickSetting:(UIButton *)settingButton{
//    NSLog(@"设置");
    
    !self.settingblock ?: self.settingblock();
    
    
}




/** ----------------------        头像相关    ----------   ***/
/**
 *  点击头部
 */
- (void)ClickHead:(UITapGestureRecognizer *)tap{
    
    __weak typeof(self) wself = self;
    
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
        [LSActionSheet showWithTitle:@"温馨提示:您可以通过手机拍照,\n从相册中选取照片作为头像,还可以查看原图" destructiveTitle:nil otherTitles:@[@"打开相机",@"从相册上传",@"查看图片"] block:^(int index) {
            NSLog(@"-----%d",index);
            if (index==0) {
                NSLog(@"打开相机");
                [wself presentViewController:ImagePickerStyleCamera];
            }else if (index==1){
                NSLog(@"从相册上传");
                [wself presentViewController:ImagePickerStylePhotoLibrary];
            }else if (index==2){
                NSLog(@"查看图片");
                [SCAvatarBrowser showImageView:self.photoImageView];
            }
            
        }];
    }else{
        [LSActionSheet showWithTitle:@"退出后不会删除任何历史数据，下次登录依然可以使用本账号。" destructiveTitle:nil otherTitles:@[@"从相册上传",@"查看图片"] block:^(int index) {
            NSLog(@"-----%d",index);
            if(index==0){
                NSLog(@"从相册上传");
                [wself presentViewController:ImagePickerStylePhotoLibrary];
            }else if (index==1){
                NSLog(@"查看图片");
                 [SCAvatarBrowser showImageView:self.photoImageView];
            }
            
        }];
    }
}
/**
 *  相片代理方法
 */
-(void)imageChooseFinishedWithImage:(UIImage *)image
{
    self.photoImageView.image = image;
    //保存到本地
    [CycleCameraAndLibraryVC saveImageToSandbox:image andImageName:userImageNamePath andResultBlock:^(BOOL success) {
        if (success) {
            NSLog(@"保存成功");
            
        }else{
            NSLog(@"保存失败");
        }
    }];
}
//弹出选择控制器
- (void)presentViewController:(imagePickerStyle)style
{
    CycleCameraAndLibraryVC *pickerVC = [[CycleCameraAndLibraryVC alloc]initWithImagePickerStyle:style];
    pickerVC.CycleDelegate = self;
    [self.vc presentViewController:pickerVC animated:YES completion:nil];

}

/** ----------------------        / 头像相关    ----------   ***/

@end
