//
//  tableHeaderView.h
//  showNav
//
//  Created by dihuijun on 17/5/9.
//  Copyright © 2017年 Cycle. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ViewController;

typedef void(^shareBlock)();
typedef void(^settingBlock)();



@interface tableHeaderView : UIView
/** 导航栏部分 **/

/**   navView   */
@property (nonatomic, strong) UIView *navBarView;
/**   rightbtnShare   */
@property (nonatomic, strong) UIButton *rightbtnShare;
/**   rightbtnSetting   */
@property (nonatomic, strong) UIButton *rightbtnSetting;
/**   titleLabel   */
@property (nonatomic, strong) UILabel *titleLabel;

/**   array储存需要添加在父容器里面的控件   */
@property (nonatomic, strong) NSMutableArray *childArray;

/**    点击分享的block */
@property (nonatomic, copy) shareBlock shareblock;
/**    点击设置的block */
@property (nonatomic, copy) settingBlock settingblock;


/** ----------------------        头像相关    ----------   ***/
///照相需要弹出父容器
@property (nonatomic, strong) ViewController *vc;
/** ----------------------        /头像相关    ----------   ***/



/**       身体部分    **/
/**  头像背景图片 */
@property(nonatomic, strong)UIImageView     *headerBackView;
/**  头像图片 */
@property(nonatomic, strong)UIImageView     *photoImageView;
/**  用户名label */
@property(nonatomic, strong)UILabel         *userNameLabel;
/**  简介label */
@property(nonatomic, strong)UILabel         *introduceLabel;







@end
