//
//  CycleTool.h
//  CDSBZX
//
//  Created by dihuijun on 17/3/2.
//  Copyright © 2017年 Cycle. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef enum{
    NONewVersion=0,//没有新版本
    HaveNewVersion=1 //新版本
    
}result;
@interface CycleTool : NSObject

/**
 * 判断是否登录
 */
+ (BOOL)isLogin;
@end
