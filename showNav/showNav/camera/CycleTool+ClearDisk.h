//
//  CycleTool+ClearDisk.h
//  CDSBZX
//
//  Created by dihuijun on 17/4/27.
//  Copyright © 2017年 Cycle. All rights reserved.
//  7.处理缓存

#import "CycleTool.h"
typedef void(^cleanCacheBlock)();
@interface CycleTool (ClearDisk)
///**  计算单个文件大小 */
//+ (float)fileSizeAtPath:(NSString *)path;
///**  计算目录大小 */
//+ (float)folderSizeAtPath:(NSString *)path;
//
///**清理缓存文件*/
//+(void)clearCache:(NSString *)path;






/*********  很有用  ********/

/**
 *  清理缓存
 */
+(void)cleanCache:(cleanCacheBlock)block;

/**
 *  整个缓存目录的大小
 */
+(float)folderSizeAtPath;

@end
