//
//  CycleCameraAndLibraryVC.h
//  CDSBZX
//
//  Created by dihuijun on 17/4/27.
//  Copyright © 2017年 Cycle. All rights reserved.
//  8.照相机和相册

#import <UIKit/UIKit.h>
typedef NS_ENUM(NSUInteger,imagePickerStyle) {
    
    ImagePickerStyleCamera,
    ImagePickerStylePhotoLibrary
    
};
/**
 * 保存成功的回调
 * @param success 保存成功的block
 */
typedef void(^resultBlock)(BOOL success);




/**** 代理  协议与委托 *****/
@protocol CycleCameraAndLibraryVCDelegate <NSObject>

- (void)imageChooseFinishedWithImage:(UIImage *)image;

@end



@interface CycleCameraAndLibraryVC : UIImagePickerController

/**   代理   */
@property (nonatomic, assign) id<CycleCameraAndLibraryVCDelegate> CycleDelegate;




/**
 *  1.初始化CycleCameraAndLibraryVC
 *
 *  @param style 打开照相机活着图库
 *
 *  @return 初始化CycleCameraAndLibraryVC
 */
- (instancetype)initWithImagePickerStyle:(imagePickerStyle)style;


/**
 *  2.保存图片到沙盒
 *
 *  @param image     要保存的图片
 *  @param imageName 保存的图片名称
 *  @param block     保存成功的值
 */
+ (void)saveImageToSandbox:(UIImage *)image andImageName:(NSString *)imageName andResultBlock:(resultBlock)block;



/**
 *  3.沙盒中获取到的照片
 *
 *  @param imageName 读取的照片名称
 *
 *  @return 从沙盒读取到的照片
 */
+ (UIImage *)loadImageFromSandbox:(NSString *)imageName;


/**
 *  4.根据文件获取沙盒路径
 *
 *  @param fileName 文件名称
 *
 *  @return 文件在沙盒中的路径
 */
+ (NSString *)filePath:(NSString *)fileName;




















@end
